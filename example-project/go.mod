module geeks-accelerator/oss/saas-starter-kit/example-project

require (
	github.com/GuiaBolso/darwin v0.0.0-20170210191649-86919dfcf808 // indirect
	github.com/Masterminds/squirrel v1.1.0 // indirect
	github.com/aws/aws-sdk-go v1.19.33
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/dimfeld/httptreemux v5.0.1+incompatible
	github.com/geeks-accelerator/sqlxmigrate v0.0.0-20190527223850-4a863a2d30db
	github.com/gitwak/gondolier v0.0.0-20190521205431-504d297a6c42 // indirect
	github.com/gitwak/sqlxmigrate v0.0.0-20190527063335-e98d5d44fc0b
	github.com/go-playground/locales v0.12.1
	github.com/go-playground/universal-translator v0.16.0
	github.com/go-redis/redis v6.15.2+incompatible
	github.com/golang/protobuf v1.3.1 // indirect
	github.com/google/go-cmp v0.2.0
	github.com/hashicorp/golang-lru v0.5.1 // indirect
	github.com/huandu/go-sqlbuilder v1.4.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/kelseyhightower/envconfig v1.3.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/leodido/go-urn v1.1.0 // indirect
	github.com/lib/pq v1.1.2-0.20190507191818-2ff3cb3adc01
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/opentracing/opentracing-go v1.1.0 // indirect
	github.com/openzipkin/zipkin-go v0.1.1 // indirect
	github.com/pborman/uuid v0.0.0-20180122190007-c65b2f87fee3
	github.com/philhofer/fwd v1.0.0 // indirect
	github.com/philippgille/gokv v0.5.0 // indirect
	github.com/pkg/errors v0.8.1
	github.com/sethgrid/pester v0.0.0-20190127155807-68a33a018ad0
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/tinylib/msgp v1.1.0 // indirect
	go.opencensus.io v0.14.0
	golang.org/x/crypto v0.0.0-20190513172903-22d7a77e9e5f
	golang.org/x/net v0.0.0-20190522155817-f3200d17e092 // indirect
	golang.org/x/sys v0.0.0-20190526052359-791d8a0f4d09 // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/tools v0.0.0-20190525145741-7be61e1b0e51 // indirect
	google.golang.org/appengine v1.6.0 // indirect
	gopkg.in/DataDog/dd-trace-go.v1 v1.14.0
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.29.0
	gopkg.in/mgo.v2 v2.0.0-20180705113604-9856a29383ce
	gopkg.in/yaml.v2 v2.2.1 // indirect
)
